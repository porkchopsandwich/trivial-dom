export const parent = <T extends Element>(target: Element): T | undefined => {
    /* tslint:disable-next-line:strict-boolean-expressions */
    return (target.parentNode as T) || undefined;
};
