/* tslint:disable:no-void-expression */

import {
    getAttr,
    getForcedAttr,
    getIntAttr,
    removeAttr,
    setAttr,
    setAttrs,
} from "../attr";

test("Setting and getting attribute.", () => {
    const target = document.createElement("div");
    const key = "id";
    const value = "test-value";
    expect(setAttr(target, key, value)).toBeUndefined();
    const retrieved = getAttr(target, key);
    expect(retrieved).toBe(value);
});

test("Getting unset attribute.", () => {
    const target = document.createElement("div");
    const key = "id";
    const retrieved = getAttr(target, key);
    expect(retrieved).toBeUndefined();
});

test("Setting many attributes at once.", () => {
    const target = document.createElement("div");
    const attrs = {
        id: "test-id",
        name: "test-name",
    };
    expect(setAttrs(target, attrs)).toBeUndefined();

    expect(getAttr(target, "id")).toBe("test-id");
    expect(getAttr(target, "name")).toBe("test-name");
    expect(getAttr(target, "class")).toBe(undefined);
});

test("Forcing attribute values.", () => {
    const target = document.createElement("div");
    const key = "id";
    const value = "test-value";
    const default_value = "default";
    let retrieved = getForcedAttr(target, key, default_value);
    expect(retrieved).toBe(default_value);
    setAttr(target, key, value);
    retrieved = getForcedAttr(target, key, default_value);
    expect(retrieved).toBe(value);
});

test("Removing attributes.", () => {
    const key = "id";
    const value = "test-value";
    const target = document.createElement("div");
    setAttr(target, key, value);
    expect(getAttr(target, key)).toBe(value);
    removeAttr(target, key);
    expect(getAttr(target, key)).toBeUndefined();
});

test("Getting int attributes", () => {
    const key = "data-num";
    const value = 50;
    const target = document.createElement("div");
    setAttr(target, key, `${value}`);
    expect(getAttr(target, key)).toBe(`${value}`);
    expect(getIntAttr(target, key)).toBe(value);
});
