/* tslint:disable:no-void-expression */

import { append, appendText } from "../append";

test("Appending nodes.", () => {
    const target = document.createElement("div");

    expect(target.children.length).toBe(0);

    const child = document.createElement("span");
    append(target, child);

    expect(target.children.length).toBe(1);
    expect(target.children[0]).toBe(child);

    const child_2 = document.createElement("h1");
    append(target, child_2);

    expect(target.children.length).toBe(2);
    expect(target.children[1]).toBe(child_2);
});

test("Append same node twice only attaches it once.", () => {
    const target = document.createElement("div");
    const child = document.createElement("span");
    append(target, child);
    append(target, child);

    expect(target.children.length).toBe(1);
    expect(target.children[0]).toBe(child);
});

test("Appending text.", () => {
    const target = document.createElement("h2");
    const message = "Test Message";
    appendText(target, message);

    expect(target.innerHTML).toBe(message);
});
