/* tslint:disable:no-void-expression */

import { getFloatVal, getIntVal, getVal, setVal } from "../val";

test("Get and Set value on invalid type", () => {
    const element = document.createElement("div");
    const test_value = "Test Value";

    expect(setVal(element as any, test_value)).toBeUndefined();

    const set_value = getVal(element as any);

    expect(set_value).toBe(test_value);
});

test("Get and Set Text Value on <input>", () => {
    const element = document.createElement("input");
    const test_value = "Test Value";

    expect(setVal(element, test_value)).toBeUndefined();

    const set_value = getVal(element);

    expect(set_value).toBe(test_value);
});

test("Get and Set Int Value", () => {
    const element = document.createElement("input");
    const test_value = 1;

    expect(setVal(element, `${test_value}`)).toBeUndefined();

    const set_value = getIntVal(element);

    expect(set_value).toBe(test_value);
});

test("Get and Set Float Value", () => {
    const element = document.createElement("input");
    const test_value = Math.PI;

    expect(setVal(element, `${test_value}`)).toBeUndefined();

    const set_value = getFloatVal(element);

    expect(set_value).toBeCloseTo(test_value);
});
