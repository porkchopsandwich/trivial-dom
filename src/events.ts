/* tslint:disable:completed-docs */

import * as jsu from "handle-events";

type IEventListener<TEvent extends Event> = (event: TEvent) => void;

interface IChainable {
    on<TEvent extends Event>(
        eventns: string,
        listener: IEventListener<TEvent>,
        capture?: boolean,
    ): this;
    onDelegate<TEvent extends Event>(
        eventns: string,
        selector: string,
        listener: IEventListener<TEvent>,
        capture?: boolean,
    ): this;
    one<TEvent extends Event>(
        eventns: string,
        listener: IEventListener<TEvent>,
        capture?: boolean,
    ): this;
    oneDelegate<TEvent extends Event>(
        eventns: string,
        selector: string,
        listener: IEventListener<TEvent>,
        capture?: boolean,
    ): this;
    off(eventns?: string, listener?: IEventListener<Event>): this;
}

export const on = <TEvent extends Event>(
    target: Node,
    eventns: string,
    listener: IEventListener<TEvent>,
    capture?: boolean,
): void => {
    jsu.addEventListener(target, eventns, listener, capture);
};

export const onDelegate = <TEvent extends Event>(
    target: Node,
    eventns: string,
    selector: string,
    listener: IEventListener<TEvent>,
    capture?: boolean,
): void => {
    jsu.delegate(target, eventns, selector, listener, capture);
};

export const one = <T extends Event>(
    target: Node,
    eventns: string,
    listener: IEventListener<T>,
    capture?: boolean,
): void => {
    const wrapped: IEventListener<T> = (event: T): void => {
        jsu.removeEventListener(target, eventns, wrapped);
        listener(event);
    };
    jsu.addEventListener(target, eventns, wrapped, capture);
};

export const oneDelegate = <T extends Event>(
    target: Node,
    eventns: string,
    selector: string,
    listener: IEventListener<T>,
    capture?: boolean,
): void => {
    const wrapped = (event: T): void => {
        jsu.removeEventListener(target, eventns, wrapped);
        listener(event);
    };
    jsu.delegate(target, eventns, selector, wrapped, capture);
};

export const off = (
    target: Node,
    eventns?: string,
    listener?: IEventListener<Event>,
): void => {
    jsu.removeEventListener(target, eventns, listener);
};

export const events = (target: Node, ns?: string): IChainable => {
    const createEventNamespace = (given: string): string => {
        return ns !== undefined ? `${given}.${ns}` : given;
    };

    const instance: IChainable = {
        on: <TEvent extends Event>(
            eventns: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): IChainable => {
            on(target, createEventNamespace(eventns), listener, capture);

            return instance;
        },
        onDelegate: <TEvent extends Event>(
            eventns: string,
            selector: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): IChainable => {
            onDelegate(
                target,
                createEventNamespace(eventns),
                selector,
                listener,
                capture,
            );

            return instance;
        },
        one: <TEvent extends Event>(
            eventns: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): IChainable => {
            one(target, createEventNamespace(eventns), listener, capture);

            return instance;
        },
        oneDelegate: <TEvent extends Event>(
            eventns: string,
            selector: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): IChainable => {
            oneDelegate(
                target,
                createEventNamespace(eventns),
                selector,
                listener,
                capture,
            );

            return instance;
        },
        off: (
            eventns?: string,
            listener?: IEventListener<Event>,
        ): IChainable => {
            off(
                target,
                eventns !== undefined ? createEventNamespace(eventns) : eventns,
                listener,
            );

            return instance;
        },
    };

    return instance;
};
