export const setAttr = (element: Element, key: string, value: string): void => {
    element.setAttribute(key, value);
};

export const setAttrs = (element: Element, attrs: IDOMAttributes): void => {
    for (const key in attrs) {
        if (attrs.hasOwnProperty(key)) {
            element.setAttribute(key, attrs[key]);
        }
    }
};

export const getAttr = (element: Element, key: string): string | undefined => {
    const attr = element.getAttribute(key);

    return attr === null ? undefined : attr;
};

export const getForcedAttr = (
    element: Element,
    key: string,
    alt: string,
): string => {
    const value = getAttr(element, key);

    return value === undefined ? alt : value;
};

export const getIntAttr = (element: Element, key: string): number => {
    return parseInt(`${getAttr(element, key)}`, 10);
};

export const removeAttr = (element: Element, key: string): void => {
    element.removeAttribute(key);
};
