import { parent } from "./parent";

export const remove = (element: Element): void => {
    const p = parent(element);
    if (p !== undefined) {
        p.removeChild(element);
    }
};
