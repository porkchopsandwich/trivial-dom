export const setProp = <TElement extends Element>(
    element: TElement,
    key: keyof TElement,
    value: string,
): void => {
    element[key] = value as any;
};

export const getProp = <TElement extends Element>(
    element: TElement,
    key: keyof TElement,
): string => {
    return element[key] as any;
};
