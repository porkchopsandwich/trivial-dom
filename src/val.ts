interface IHasValue extends Element {
    // the value
    value: string;
}

export const setVal = (element: IHasValue, value: string): void => {
    element.value = value;
};

export const getVal = (element: IHasValue): string => {
    return element.value;
};

export const getIntVal = (element: IHasValue): number => {
    return parseInt(getVal(element), 10);
};

export const getFloatVal = (element: IHasValue): number => {
    return parseFloat(getVal(element));
};
