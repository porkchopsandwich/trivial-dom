const readyState = /complete|loaded|interactive/;

export const ready = (
    handler: () => any,
    context: Document = document,
): void => {
    if (
        context.body instanceof Element &&
        readyState.test(context.readyState)
    ) {
        setTimeout(handler, 0);
    } else {
        context.addEventListener("DOMContentLoaded", handler, false);
    }
};
