export const append = <TTarget extends Node, TChild extends Node>(
    target: TTarget,
    ...elements: TChild[]
): void => {
    const l = elements.length;
    for (let i = 0; i < l; ++i) {
        target.appendChild(elements[i]);
    }
};

export const appendText = (target: Element, content: string): void => {
    target.insertAdjacentHTML("beforeend", content);
};
