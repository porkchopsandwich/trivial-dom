export const prepend = (target: Element, ...elements: Element[]): void => {
    const l = elements.length;
    for (let i = 0; i < l; ++i) {
        target.insertAdjacentElement("afterbegin", elements[i]);
    }
};

export const prependText = (target: Element, content: string): void => {
    target.insertAdjacentHTML("afterbegin", content);
};
