import { querySelector } from "./querySelector";

export const find = <T extends Element>(
    element: Element,
    selector: string,
): T[] => {
    const nodes: T[] = [];
    const matches = querySelector<T>(selector, element);
    const l = matches.length;
    for (let i = 0; i < l; ++i) {
        if (nodes.indexOf(matches[i]) === -1) {
            nodes.push(matches[i]);
        }
    }

    return nodes;
};

export const findOne = <T extends Element>(
    element: Element,
    selector: string,
): T | undefined => {
    const results = find<T>(element, selector);

    return results.length > 0 ? results[0] : undefined;
};

export const findForceOne = <T extends Element>(
    element: Element,
    selector: string,
): T => {
    const results = find<T>(element, selector);
    if (results.length === 0) {
        throw new ReferenceError(
            `Could not find any matches for "${selector}" in Element.`,
        );
    }

    return results[0];
};
