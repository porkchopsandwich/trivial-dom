import { parent } from "./parent";

export const before = (target: Element, to_insert: Element): void => {
    const p = parent<Element>(target);
    if (p !== undefined) {
        p.insertBefore(to_insert, target);
    }
};
