export const getText = (element: Element): string => {
    const text = element.textContent;

    return text === null ? "" : text;
};
