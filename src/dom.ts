import { querySelector } from "./querySelector";

export const dom = <TElement extends Element>(
    els: TElement[],
): IDOM<TElement> => {
    const elements = (): TElement[] => {
        return els;
    };

    return {
        elements,
    };
};

export const domSelector = <TElement extends Element>(
    selector: string,
): IDOM<TElement> => {
    return dom(querySelector<TElement>(selector));
};

export const domOne = <TElement extends Element>(
    element: TElement,
): IDOM<TElement> => {
    return dom([element]);
};
