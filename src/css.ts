/* tslint:disable:strict-boolean-expressions */

const camelize = (value: string): string => {
    return value.replace(/-([\da-z])/gi, (matches, letter: string) => {
        return letter.toUpperCase();
    });
};

const dasherize = (value: string): string => {
    return value.replace(/([a-z\d])([A-Z])/g, "$1-$2").toLowerCase();
};

export const css = (element: HTMLElement, props: IDOMStyleProperties): void => {
    const camelized: IDOMStyleProperties = {};
    for (const prop in props) {
        if (props.hasOwnProperty(prop)) {
            camelized[camelize(prop)] = props[prop];
        }
    }

    for (const prop in props) {
        if (props.hasOwnProperty(prop)) {
            const value = props[prop];
            if (value) {
                element.style[prop as any] = value;
            } else {
                element.style.removeProperty(dasherize(prop));
            }
        }
    }
};

export const show = (element: HTMLElement): void => {
    css(element, {
        display: undefined,
    });
};

export const hide = (element: HTMLElement): void => {
    css(element, {
        display: "none",
    });
};
