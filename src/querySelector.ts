/* tslint:disable:no-string-literal no-unsafe-any */

const simple_selector = /^[.#]?[\w-]*$/;

type Context = Element | Document;

const isDocument = (v: any): v is Document => {
    return (
        v &&
        typeof v["createElement"] === "function" &&
        typeof v["getElementById"] === "function"
    );
};

export const querySelector = <T extends Element>(
    selector: string,
    context: Context = document,
): T[] => {
    const is_simple = simple_selector.test(selector);

    if (is_simple) {
        if (selector[0] === "#") {
            const element: T | undefined = isDocument(context)
                ? context.getElementById(selector.slice(1))
                : (document.getElementById(selector.slice(1)) as any);

            return element instanceof Element ? [element] : [];
        }
        if (selector[0] === ".") {
            return Array.prototype.slice.call(
                context.getElementsByClassName(selector.slice(1)),
            ) as T[];
        }

        return Array.prototype.slice.call(
            context.getElementsByTagName(selector),
        ) as T[];
    }

    return Array.prototype.slice.call(
        context.querySelectorAll(selector),
    ) as T[];
};

export const querySelectorOne = <T extends Element>(
    selector: string,
    context: Context = document,
): T | undefined => {
    const result = querySelector<T>(selector, context);

    return result.length > 0 ? result[0] : undefined;
};

export const querySelectorForceOne = <T extends Element>(
    selector: string,
    context: Context = document,
): T => {
    const result = querySelector<T>(selector, context);
    if (result.length === 0) {
        throw new ReferenceError(
            `Cannot find element with selector "${selector}".`,
        );
    }

    return result[0];
};
