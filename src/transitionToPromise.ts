import { off, on } from "./events";

let c = 0;

const generateNS = (): string => {
    return `ttp${c++}`;
};

export const transitionToPromise = async (
    element: Element,
    property_name: string,
): Promise<number> => {
    return new Promise(
        (resolve): void => {
            const ns = generateNS();
            on(
                element,
                `transitionend.${ns}`,
                (e: TransitionEvent): void => {
                    if (
                        e.propertyName === property_name &&
                        e.target === element
                    ) {
                        off(element, `transitionend.${ns}`);
                        resolve(e.elapsedTime);
                    }
                },
            );
        },
    );
};
