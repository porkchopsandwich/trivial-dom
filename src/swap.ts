import { before } from "./before";
import { replaceWith } from "./replaceWith";

export const swap = <T extends HTMLElement>(
    first: T,
    second: T,
    placeholder_generator: () => T = (): T => {
        return document.createElement("span") as T;
    },
): void => {
    const first_placeholder = placeholder_generator();
    const second_placeholder = placeholder_generator();
    before(first, first_placeholder);
    before(second, second_placeholder);
    replaceWith(second_placeholder, first);
    replaceWith(first_placeholder, second);
};
