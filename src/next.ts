import { children } from "./children";
import { filter } from "./filter";
import { parent } from "./parent";

export const next = <T extends Element>(
    target: Element,
    selector?: string,
): T | undefined => {
    const p = parent<Element>(target);
    if (p === undefined) {
        return undefined;
    }

    const siblings = children<T>(p);
    const index = siblings.indexOf(target as T);
    const candidates = filter(siblings.slice(index + 1), selector);

    if (candidates.length > 0) {
        return candidates[0];
    }

    return undefined;
};
