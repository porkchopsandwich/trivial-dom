enum RuleAction {
    Add,
    Remove,
    RemoveAll,
    Toggle,
}

const isA = <T>(val: any, type: string): val is T => {
    return typeof val === type;
};

const isString = (val: any): val is string => {
    return isA<string>(val, "string");
};

const isBool = (val: any): val is boolean => {
    return isA<boolean>(val, "boolean");
};

const toRule = (name: string, action: RuleAction): IClassRule => {
    return {
        name,
        action,
    };
};

const toAddRule = (name: string): IClassRule => {
    return toRule(name, RuleAction.Add);
};

const toRemoveRule = (name: string): IClassRule => {
    return toRule(name, RuleAction.Remove);
};

const toToggleRule = (name: string): IClassRule => {
    return toRule(name, RuleAction.Toggle);
};

export const classList = (element: HTMLElement): DOMTokenList => {
    return element.classList;
};

const normalise = (unnormalised: string): string => {
    return unnormalised
        .split("!")
        .join("")
        .split("~")
        .join("");
};

const parseString = (rule: string): IClassRule => {
    let action: RuleAction = RuleAction.Add;
    let name: string = rule;

    if (rule[0] === "^") {
        action = RuleAction.RemoveAll;
        name = "";
    } else if (rule[0] === "!") {
        action = RuleAction.Remove;
        name = normalise(name);
    } else if (rule[0] === "~") {
        action = RuleAction.Toggle;
        name = normalise(name);
    }

    return {
        action,
        name,
    };
};

const parseStrings = (rules: string[]): IClassRule[] => {
    return rules.map(parseString);
};

const applyRules = (rules: IClassRule[], target: HTMLElement): void => {
    let class_list = classList(target);
    const l = rules.length;
    for (let i = 0; i < l; ++i) {
        const rule = rules[i];
        switch (rule.action) {
            case RuleAction.RemoveAll:
                target.className = "";
                class_list = classList(target);
                break;
            case RuleAction.Add:
                class_list.add(rule.name);
                break;
            case RuleAction.Toggle:
                if (class_list.contains(rule.name)) {
                    class_list.remove(rule.name);
                } else {
                    class_list.add(rule.name);
                }
                break;
            case RuleAction.Remove:
                class_list.remove(rule.name);
                break;
            default:
                throw new TypeError(
                    `Invalid rule action; ${JSON.stringify(rule)}`,
                );
        }
    }
};

const parseClassRules = (rules: RawRules): IClassRule[] => {
    if (isString(rules)) {
        return parseStrings(rules.split(" "));
    } else {
        return rules.map((rule) => {
            if (isString(rule)) {
                return parseString(rule);
            } else {
                return rule;
            }
        });
    }
};

export const classRules = (
    raw_rules: RawRules,
): ((element: HTMLElement) => void) => {
    const rules = parseClassRules(raw_rules);

    return (element: HTMLElement): void => {
        applyRules(rules, element);
    };
};

export const elementClasses = (
    element: HTMLElement,
): ((raw_rules: RawRules) => void) => {
    return (raw_rules: RawRules): void => {
        const rules = parseClassRules(raw_rules);
        applyRules(rules, element);
    };
};

export const addClasses = (
    element: HTMLElement,
    ...classes: string[]
): void => {
    applyRules(classes.map(toAddRule), element);
};

export const removeAllClasses = (element: HTMLElement): void => {
    applyRules([{ action: RuleAction.RemoveAll, name: "" }], element);
};

export const removeClasses = (
    element: HTMLElement,
    ...classes: string[]
): void => {
    applyRules(classes.map(toRemoveRule), element);
};

export const toggleClasses = (
    element: HTMLElement,
    ...classes: string[]
): void => {
    applyRules(classes.map(toToggleRule), element);
};

export const applyClasses = (
    target: HTMLElement,
    raw_rules: string | string[] | IClassRule[],
): void => {
    classRules(raw_rules)(target);
};

export const hasClass = (target: HTMLElement, class_name: string): boolean => {
    return classList(target).contains(class_name);
};

export const switchClasses = (
    element: HTMLElement,
    switcher: boolean | (() => boolean),
    ...classes: string[]
): void => {
    const result = isBool(switcher) ? switcher : switcher();
    if (result) {
        addClasses(element, ...classes);
    } else {
        removeClasses(element, ...classes);
    }
};
