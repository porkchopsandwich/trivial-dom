import { matches } from "./matches";

export const parents = <T extends Element>(
    e: Element,
    selector?: string,
): T[] => {
    const ancestors: T[] = [];

    let parent: T = e.parentNode as T;
    while (parent instanceof Element) {
        ancestors.push(parent);
        parent = parent.parentNode as T;
    }

    if (selector !== undefined) {
        return ancestors.filter((ancestor) => {
            return matches(ancestor, selector);
        });
    } else {
        return ancestors;
    }
};
