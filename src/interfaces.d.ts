declare type RuleActions = 0 | 1 | 2 | 3;

declare type RawRuleList = (string | IClassRule)[];

declare type RawRules = string | RawRuleList;

declare interface IClassRule {
    action: RuleActions;
    name: string;
}

declare interface IDOM<TElement extends Element> {
    elements(): TElement[];
}

declare module "handle-events" {
    interface IEventListener<TEvent extends Event> {
        (event: TEvent): void;
    }

    interface IJSUFacade {
        off<TEvent extends Event>(
            eventns: string,
            listener?: IEventListener<TEvent>,
        ): this;
        on<TEvent extends Event>(
            eventns: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): this;
    }

    interface IJSU {
        addEventListener<TEvent extends Event>(
            node: Node,
            eventns: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): void;
        delegate<TEvent extends Event>(
            node: Node,
            eventns: string,
            selector: string,
            listener: IEventListener<TEvent>,
            capture?: boolean,
        ): void;
        removeEventListener<TEvent extends Event>(
            node: Node,
            eventns?: String,
            listener?: IEventListener<TEvent>,
        ): void;
        handleEvents(node: Node): IJSUFacade;
    }

    const jsu: IJSU;
    export = jsu;
}

interface IDOMStyleProperties {
    [key: string]: string | undefined;
}

interface IDOMAttributes {
    [key: string]: string;
}
