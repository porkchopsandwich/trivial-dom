/* tslint:disable:no-unsafe-any */

import { matches } from "./matches";
import { parent } from "./parent";

const closestLib = <TElement extends Element>(
    element: Element,
    selector: string,
    check_self = false,
): TElement | undefined => {
    let next: any = check_self ? element : parent(element);

    while (next && next !== document) {
        if (matches(next, selector)) {
            return next;
        }
        next = parent(next);
    }

    return undefined;
};

export const closest = <T extends Element>(
    e: Element,
    selector: string,
    check_self = false,
): T | undefined => {
    return closestLib<T>(e, selector, check_self);
};

export const closestForce = <T extends Element>(
    e: Element,
    selector: string,
    check_self = false,
): T => {
    const c = closest<T>(e, selector, check_self);
    if (c === undefined) {
        throw new ReferenceError(
            `Could not find any matching ancestors with selector "${selector}"`,
        );
    }

    return c;
};
