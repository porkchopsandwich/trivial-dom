import { appendText } from "./append";
import { empty } from "./empty";

export const setText = (element: Element, content: string): void => {
    empty(element);
    appendText(element, content);
};
