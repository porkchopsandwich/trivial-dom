export const each = <T extends Element>(
    callback: (element: T, index: number, collection: HTMLCollection) => void,
    collection: HTMLCollection,
): void => {
    const length = collection.length;
    for (let i = 0; i < length; i++) {
        callback(collection[i] as any, i, collection);
    }
};
