import { children } from "./children";
import { parent } from "./parent";

export const indexOf = (element: Element): number | undefined => {
    const p = parent<Element>(element);
    if (p === undefined) {
        return undefined;
    } else {
        const candidates = children(p);

        const index = candidates.indexOf(element);

        return index === -1 ? undefined : index;
    }
};
