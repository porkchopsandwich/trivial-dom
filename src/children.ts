import { each } from "./each";
import { matches } from "./matches";

export const children = <T extends Element>(
    element: Element,
    selector?: string,
): T[] => {
    const nodes: T[] = [];

    if (element.children.length > 0) {
        each<T>((child) => {
            if (selector === undefined || matches(child, selector)) {
                nodes.push(child);
            }
        }, element.children);
    }

    return nodes;
};
