/* tslint:disable:no-unsafe-any strict-type-predicates */

type Matches = (element: Element, selector: string) => boolean;

const matchesFactory = (): Matches => {
    const context: any =
        typeof Element !== "undefined" ? Element.prototype : window;
    const matches_selector: (selector: string) => boolean =
        context.matches ||
        context.matchesSelector ||
        context.mozMatchesSelector ||
        context.msMatchesSelector ||
        context.oMatchesSelector ||
        context.webkitMatchesSelector;

    return (element: Element, selector: string): boolean => {
        return matches_selector.call(element, selector);
    };
};

export const matches = matchesFactory();
