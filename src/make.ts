import { appendText } from "./append";
import { setAttr } from "./attr";
import { addClasses } from "./classes";
import { css } from "./css";

const step = 2;

/**
 * Generates a DOMtastic instance of a new Element described by the passed selector.
 *
 * @example $n<HTMLDivElement>("div#el-id.el-class-1.el-class-2") => <div id="el-id" class="el-class-1 el-class-2" />
 *
 * @template TElement   The Element type that the returned DOMtastic instance will contain.
 *
 * @param  {string} selector        The selector of the element to create.
 * @param {IDOMAttributes} attributes   A dictionary of elements.
 * @param {string} text             The text content of the element.
 * @param {IDOMStyleProperties} styles           A dictionary of styles.
 *
 * @return {TElement}   Return the created element.
 */
export const make = <TElement extends HTMLElement>(
    selector: string,
    attributes: IDOMAttributes = {},
    text?: string,
    styles: IDOMStyleProperties = {},
): TElement => {
    const [tag_name, ...selectors] = selector.split(/([\.\#])/i);

    const element: TElement = document.createElement(tag_name) as any;

    for (let i = 0; i < selectors.length; i += step) {
        const operator = selectors[i].trim();
        const value = i < selectors.length - 1 ? selectors[i + 1].trim() : "";
        switch (operator) {
            case "#":
                setAttr(element, "id", value);
                break;
            case ".":
                addClasses(element, value);
                break;
            default:
                break;
        }
    }

    for (const key in attributes) {
        if (attributes.hasOwnProperty(key)) {
            setAttr(element, key, attributes[key]);
        }
    }

    if (text !== undefined) {
        appendText(element, text);
    }

    if (Object.keys(styles).length > 0) {
        css(element, styles);
    }

    return element;
};
