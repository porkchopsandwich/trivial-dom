import { before } from "./before";
import { remove } from "./remove";

export const replaceWith = (target: Element, replace_with: Element): void => {
    before(target, replace_with);
    remove(target);
};
