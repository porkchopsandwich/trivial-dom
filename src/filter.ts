import { matches } from "./matches";

export const filter = <T extends Element>(
    elements: T[],
    selector?: string,
): T[] => {
    return elements.filter((candidate) => {
        return selector === undefined || matches(candidate, selector);
    });
};
